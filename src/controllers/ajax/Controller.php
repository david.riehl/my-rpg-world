<?php
namespace MyRpgApp\controllers\ajax;

class Controller
{
    public static function defaultAction()
    {
        echo json_encode(func_get_args());
    }
}