<?php
namespace MyRpgApp\controllers\error;

use MyRpgApp\core\View;

class Controller
{
    public static function pageNotFoundAction() {
        View::forceTemplate("error");
        View::addParameter("error_title", "404");
        View::addParameter("error_message", "Page not found.");
        View::display();
    }

    public static function accessDeniedAction() {
        View::forceTemplate("error");
        View::addParameter("error_title", "403");
        View::addParameter("error_message", "Access Denied.");
        View::display();
    }

    public static function databaseAccessErrorAction() {
        $details = filter_input(INPUT_GET, "details", FILTER_SANITIZE_STRING);
        View::forceTemplate("error");
        View::addParameter("error_title", "500");
        View::addParameter("error_message", "Database Access Error.");
        View::addParameter("error_details", $details);
        View::display();
    }
}