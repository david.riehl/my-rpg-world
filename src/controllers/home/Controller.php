<?php
namespace MyRpgApp\controllers\home;

use MyRpgApp\core\Database;
use MyRpgApp\core\View;

class Controller
{
    public static function homeAction()
    {
        $args = func_get_args();
        View::addParameter("args", $args);
        View::display();
    }
}