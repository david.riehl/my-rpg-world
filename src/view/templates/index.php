<?php
namespace MyRpgApp\view\templates;
use MyRpgApp\core\Uri;
?><!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- IE -->
        <link rel="shortcut icon" type="image/png" href="<?= Uri::getRoot(); ?>/assets/NicePng_crossed-swords-png_574056.png" />
        <!-- other browsers -->
        <link rel="icon" type="image/png" href="<?= Uri::getRoot(); ?>/assets/NicePng_crossed-swords-png_574056.png" />

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <title>My RPG World !</title>
        <script>document.uriRoot = '<?= Uri::getRoot(); ?>';</script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<?php if (file_exists("src/view/css/${template}.css")): ?>
        <link href="<?= Uri::getRoot()."/src/view/css/${template}.css"?>" rel="stylesheet">
<?php endif; ?>
    </head>
    <body class="bg-dark">
<?php require_once("navbar.php"); ?>
        <main class="main container bg-light pt-3 pb-1 mb-3" style="margin-top: 5em;">
<?php require_once($template.".php"); ?>
<?php if (file_exists("src/view/js/${template}.js")): ?>
        <script src="<?= Uri::getRoot()."/src/view/js/${template}.js"?>"></script>
<?php endif; ?>
                <footer>
                        &copy; David RIEHL &lt;david&#x2E;riehl[at]gmail&#x2E;com&gt;
                </footer>
        </main>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    </body>
</html>