<?php
namespace MyRpgApp\view\templates;
use MyRpgApp\core\Uri;
?><nav class="navbar fixed-top navbar-dark container" style="background-color: black;">
    <a class="navbar-brand" href="#">
        <img src="<?= Uri::getRoot(); ?>/assets/NicePng_crossed-swords-png_574056.png" width="30" height="30" class="d-inline-block align-top" alt="">
        My RPG World
    </a>
<?php require_once "navbar-login.php" ?>
</nav>
