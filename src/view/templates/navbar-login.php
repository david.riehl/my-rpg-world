<?php
namespace MyRpgApp\view\templates;
use MyRpgApp\core\Uri;
?>    <div class="btn-group">
        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Account
        </button>
        <div class="dropdown-menu dropdown-menu-end">
        <form class="px-4 py-3">
            <div class="mb-1">
            <input type="email" class="form-control" placeholder="email@example.com" style="width: 15em;">
            </div>
            <div class="mb-1">
            <input type="password" class="form-control" placeholder="Password">
            </div>
            <div class="mb-1">
            <div class="form-check">
                <input type="checkbox" class="form-check-input" style="margin-left:-1.3em;" id="dropdownCheck">
                <label class="form-check-label" for="dropdownCheck">
                Remember me
                </label>
            </div>
            </div>
            <div class="text-right">
                <button type="submit" class="btn btn-outline-secondary">Sign in</button>
            </div>
        </form>
        <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#"><small>First time here? Create your account</small></a>
            <form class="form-inline" action="<?= Uri::getRoot(); ?>/forgot" method="POST">
            <button class="dropdown-item btn btn-link" href="#"><small>Forgot your password?</small></button>
            </form>
        </div>
    </div>
