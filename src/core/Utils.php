<?php
namespace MyRpgApp\core;

class Utils
{
    private static $namespace;
    
    public static function getNamespace() {
        if (! isset(self::$namespace)) {
            self::$namespace = explode("\\", __NAMESPACE__)[0];
        }
        return self::$namespace;
    }
}