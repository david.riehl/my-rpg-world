<?php
namespace MyRpgApp\core;

use PDO;
use PDOException;

class Database
{
    private static $dsn;
    private static $username;
    private static $password;
    private static $pdo;

    private static function setConfig() {
        $json = file_get_contents("src/database.json");
        $object = json_decode($json);
        $driver = $object->dsn->driver;
        $dbname = $object->dsn->dbname;
        $host = $object->dsn->host;
        $port = $object->dsn->port;
        $charset = $object->dsn->charset;
        self::$dsn = "${driver}:dbname=${dbname};host=${host};port=${port};charset=${charset};";
        self::$username = $object->username;
        self::$password = $object->password;
    }

    private static function unsetConfig() {
        self::$dsn = null;
        self::$username = null;
        self::$password = null;
    }

    public static function connect() {
        global $DEBUG_MODE;
        self::setConfig();
        try {
            self::$pdo = new PDO(self::$dsn, self::$username, self::$password);
        } catch (PDOException $e) {
            $root = Uri::getRoot();
            $url = "${root}/error/database/access";
            if ($DEBUG_MODE) {
                $message = $e->getMessage();
                $url .= "?details=${message}";
            }
            header("Location: ${url}");
        }
        self::unsetConfig();
    }

    public static function disconnect() {
        self::$pdo = null;
    }

    public static function getPDO() {
        if (self::$pdo == null) { self::connect(); }
        return self::$pdo;
    }
}