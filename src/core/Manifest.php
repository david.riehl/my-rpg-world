<?php
namespace MyRpgApp\core;

use MyRpgApp\controllers\error\Controller;

final class Manifest extends Entity
{
    private static $manifest = null;
    private static $whiteList = null;
    private static $default = null;

    private static function read_manifest() {
        $json = file_get_contents("src/manifest.json");
        self::$manifest = json_decode($json);
    }

    public static function getWhiteList() {
        if (self::$whiteList == null) { 
            self::setWhiteList(); 
        }
        return self::$whiteList;
    }

    private static function setWhiteList() {
        if (self::$manifest == null) { 
            self::read_manifest(); 
        }
        self::$default = self::$manifest->default;
        foreach(self::$manifest->whiteList as $route) {
            $controller = $route->controller;
            $action = $route->action;
            self::$whiteList[] = "${controller}::${action}";
        }
    }

    public static function getDefault() {
        if (self::$default == null) { 
            self::setDefault(); 
        }
        return self::$default;
    }

    private static function setDefault() {
        if (self::$manifest == null) { 
            self::read_manifest(); 
        }
        self::$default = self::$manifest->default;
    }

    public static function checkRoute($route) {
        $found = false;
        $count_whiteList = count(self::getWhiteList());
        $controller = $route->getController();
        $action = $route->getAction();
        $searched = "${controller}::${action}";

        $found = in_array($searched, self::$whiteList, true);

        if ($found) {
            $controller = $route->getController();
            $action = $route->getAction();
        } else {
            $default = self::getDefault();
            $controller = $default->controller;
            $action = $default->action;
        }

        $namespace = Utils::getNamespace();
        $class = "${namespace}\\controllers\\${controller}\\Controller";
        $method = "${action}Action";
        $callback = [$class, $method];

        return $callback;
    }
}