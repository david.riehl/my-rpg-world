# TODO

- Credentials (login, password, key = login + password + datetime session)
- ORM (Object Relational Mapping)
- Security (check user privileges)
- Session (start, destroy, login, logout)
- User (generic user info id, ip, status)
- UserPrivilege (user privileges)
- UserStatus (Connected, Idle, Busy, DoNotDisturb, Disconnected)
